class FileSize:
    """ Class for filesize and its representation """
    def __init__ (self, sz=0):
        self.size = sz
        self.human = self.bytes_to_human()

    def bytes_to_human (self, factor=1024.):
        """NO ERROR CONTROL!! Convert file size to human readable"""
        units = ['bytes', 'KB', 'MB', 'GB', 'TB', 'PB']
        for i in xrange(1, len(units)+1):
            if self.size < factor ** i or i >= len(units):
                return('%.1f %s') % ((self.size / (factor ** (i-1))), units[i-1])

    def set_size(self, sz):
        """ Set a size """
        self.size = sz
        self.human = self.bytes_to_human()
