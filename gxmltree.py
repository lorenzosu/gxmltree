#!/usr/bin/env python
"""
gxmltree - Simple yet effective Gtk XML file tree viewer.

Some parts inspired by pyGtk tutorial example by John Finlay 
http://www.moeraki.com/pygtktutorial
and the pyGtk and FAQ tutorial available at:
http://www.pygtk.org/pygtk2tutorial/
http://faq.pygtk.org/

Also the lxml documentation was precious:
Lorenzo Sutton - lorenzofsutton[remove_th1s_if human]@gmail.com
"""
"""
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import os
import sys
import cgi
from collections import defaultdict
try:
    import pygtk
    pygtk.require('2.0')
    import gtk
    from lxml import etree
except ImportError:
    print "You need pygtk 2.0, gtk and lxml installed"
    sys.exit(1)

import FileSize

def pango_color_span (string, color=None):
    """ Create a gtk pango span with foreground color specified by color
    use cgi.escape to escape nasty chars."""
    fore_attrib = ''
    if color != None:
        fore_attrib =  ' foreground="'+color+'"'
    return_string = ('<span%s>%s</span>') % (fore_attrib, cgi.escape(string))   
    return return_string

def make_attribute_pango (name, val, name_color, value_color):
    """ Make pango coloured attribute string """
    return_string = ''
    name = (' %s') %(name)
    name = pango_color_span(name, name_color)
    val = ('"%s"') % (val)
    val = pango_color_span(val, value_color)
    
    return_string += name + '=' + val
    return return_string
    


def clean_lxml_namespace (the_string, namespace_dict):
    """ Hack to 'clean' the namespaces and reintroduce namespace prefixes.
    In fact lxml "avoids namespace prefixes" (see http://lxml.de/tutorial.htm)
    But here we prefer prefixes for the representation of the xml.
    """
    if the_string.find('{') < 0:
        return the_string
    this_namespace = the_string[the_string.find('{') + 1:the_string.find('}')]
    tail = ':'
    if namespace_dict[this_namespace][0] == '':
        tail = ''
    # HACK: Why? We just take the first one.. there could however be more...
    return_string = the_string.replace('{' + this_namespace + '}',
                                   namespace_dict[this_namespace][0]+tail)
    return return_string

def uri_to_filename(uri):
    """ Convert an uri (as returned bu drag and drop) to a filename """
    # TODO support windows and make it nicer..
    return uri.replace('file://','')

class CounterClass:
    """ Very simple 1-step counter with optional initial value """
    def __init__ (self, start=0):
        self.value = start
    def reset (self):
        """ reset the counter to 0 """
        self.value = 0
    def oneup (self):
        """ add one to the counter """
        self.value = self.value + 1
    def getvalue (self):
        """ get the value """
        return self.value

class MainWindow:
    """ Class for the main window """
    def recurse_append (self, iter_tree, xml_elements, namespace_dic):
        """
        Recursively append xml (lxml elements) to a pygtk tree structure.
        The representation is NOT xml syntax but tries to be familiar.
        ProgressBar is updated every update_num nodes, and the whole Window is
        also updated continuosly so that it is responsive while the tree
        is being created. This is very useful for big XML files, so that the
        user can start using the parts already generated while the rest is
        being created.
        """
        # in case the user selected the cancel button
        if self.cancel:
            while gtk.events_pending():
                gtk.main_iteration()
                return
        xml = xml_elements.getchildren()
        # counter of nodes for progrssbar...
        self.counter.oneup()
        self.progressbar.set_fraction(float(self.counter.getvalue()) /
                                            self.total_xml_nodes)
        self.progressbar.set_text(str(self.counter.getvalue()) +
                              ' of ' + str(self.total_xml_nodes) +
                              ' nodes added')
        # update the UI and progress bar every 2000 nodes
        update_num = 2000
        if self.xml_filesize.size >= 50000000: # ~ 50 MB
            update_num = 500
        while (
                (
                    self.counter.getvalue() == 1 or
                    self.counter.getvalue() % update_num == 0
                )
                and 
                gtk.events_pending()
              ):
            gtk.main_iteration()
            
        # process all the children in the xml element
        for element in xml:
            # make a pretty (pango) string for the tag
            tag_string = clean_lxml_namespace(element.tag, namespace_dic)
            tag_string = pango_color_span(tag_string, self.tag_color)
            # make a pretty (pango) string for all the attributes
            att_string = ''
            for attrib in element.attrib:
                name = clean_lxml_namespace(attrib, namespace_dic)
                val = element.attrib[attrib]
                att_string += make_attribute_pango(name, val, 
                                                self.attrib_color, 
                                                self.value_color)
            if len(element.attrib) > 0:
                att_string += '"'

            # make a string fot the text (left and right whitespace removed)
            text_string = ''
            if element.text != None:
                text_string += str(element.text)
            # take into account lxml tail behaviour quirks
            # (i.e. check all the tails
            for child in element.getchildren():
                if child.tail != None:
                    text_string += str(child.tail)
            text_string = text_string.lstrip()
            text_string = text_string.rstrip()
            att_string = att_string[:-1]
            # make the final string that will be added to the tree
            if len(element) >= 0:
                final_string = ''.join(pango_color_span('<') +
                                       tag_string +
                                       att_string +
                                       pango_color_span('>') +
                                       pango_color_span(text_string) +
                                       pango_color_span('</') +
                                       tag_string +
                                       pango_color_span('>')
                        )
            # This in case the tag has no children
            else:
                final_string = ''.join(pango_color_span('<') +
                                     tag_string +
                                     att_string +
                                     pango_color_span('/>')
                                )
            # append to the tree the final_string and
            this_iter = self.treestore.append(iter_tree,
                                  [("%s" % (final_string)),
                                  etree.ElementTree(element).getpath(element)])
            # call function recorsively this time on the generated this_iter
            self.recurse_append(this_iter, element, namespace_dic)


    def delete_event(self, widget, event, data=None):
        """ Close window and quit. The sys.exit is needed in case the user
        quits while the XML tree is being generated to avoid hanging"""
        self.cancel = True
        gtk.main_quit()
        sys.exit(0)
        return False

    def capture_keypress (self, widget, event, data=None):
        """Capture CTRL+Q keypress to quit """
        keyval = event.keyval
#        name = gtk.gdk.keyval_name(keyval)
        keylabel = str(gtk.accelerator_get_label(keyval, event.state))
        if keylabel == 'Ctrl+Mod2+Q' or keylabel == 'Ctrl+Q':
            self.delete_event(self.main_window, self.delete_event)

    def chose_file (self, widget, data=None):
        """ Dialog for choosing the XML file """
        dialog = gtk.FileChooserDialog("Open..", None,
                                       gtk.FILE_CHOOSER_ACTION_OPEN,
                                       (gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL,
                                        gtk.STOCK_OPEN, gtk.RESPONSE_OK))
        dialog.set_default_response(gtk.RESPONSE_OK)
        filter = gtk.FileFilter()
        filter.set_name("XML")
        filter.add_mime_type("text/xml")
        filter.add_pattern("*.xml")
        filter.add_pattern("*.XML")
        dialog.add_filter(filter)
        filter = gtk.FileFilter()
        filter.set_name("All files")
        filter.add_pattern("*")
        dialog.add_filter(filter)
        if self.xml_file != '':
            dialog.set_filename (self.xml_file)
        response = dialog.run()
        if response == gtk.RESPONSE_OK:
            self.xml_file = dialog.get_filename()
            self.xml_filesize.set_size(os.path.getsize(self.xml_file))
            self.button_create.set_sensitive(True)
            self.button_create.set_tooltip_text('Create XML tree')            
        elif response == gtk.RESPONSE_CANCEL:
            pass
        dialog.destroy()

    def tree_change_callback (self, widget):
        sel = widget.get_selection()
        (model, iterator) = sel.get_selected()
        if iterator != None:
            self.current_xpath = self.treestore.get_value(iterator, 1)
            self.entry_path.set_text(self.current_xpath)
        
        
    def create_button_callback (self, widget, data=None):
        """ Create Tree button is clicked"""
        # In case no xml file has been selected
        if self.xml_file == '':
            return
        # Change the cursor for main_window to gtk.WATCH
        watch_cursor = gtk.gdk.Cursor(gtk.gdk.WATCH)
        self.main_window.window.set_cursor(watch_cursor)        
        self.tvcolumn.set_title('Adding nodes to tree...')
        # This update method pointed out by Juhaz on #pygtk irc.gnome.org
        while gtk.events_pending():
            gtk.main_iteration()
        # Call the create_tree method
        self.create_tree()
        # Set the cursor back to normal once done
        self.main_window.window.set_cursor(None)

    def cancel_button_callback (self, widget, data=None):
        """ If user clickes the cancel button self.cancel is checked at each
        iteration of the recursive function """
        self.cancel = True
        
    def create_tree (self):
        """ Create tree from the XML file """
        try:
            parser = etree.XMLParser(ns_clean=True, remove_comments=True)
            self.xml_etree = etree.parse(self.xml_file, parser)
        # In case parsing fails tell the user and return
        except :
            self.scrolled_window.window.set_cursor(None)
            parse_error_dialog = gtk.MessageDialog(None,
                          gtk.DIALOG_DESTROY_WITH_PARENT, gtk.MESSAGE_ERROR, 
                          gtk.BUTTONS_CLOSE, "Error parsing file %s"
                          % self.xml_file)
            parse_error_dialog.run()
            parse_error_dialog.destroy()
            return

        self.main_window.set_title("gxmltree - %s" % (self.xml_file))
        self.treestore.clear()
        while gtk.events_pending():
            gtk.main_iteration()        
        self.xml_root = self.xml_etree.getroot()
        self.total_xml_nodes = len(self.xml_root.xpath('//*'))
        self.progressbar.set_fraction(0.0)
        # generate swapped namespace dictionary
        self.namespace = self.xml_root.nsmap
        self.namespace_dic = defaultdict(list)
        for key, val in self.namespace.items():
            if key == None:
                key = ''
            self.namespace_dic[val].append(key)
        # Tag and attribute strings for root element
        tag_string = clean_lxml_namespace(self.xml_root.tag, 
                                            self.namespace_dic)
        tag_string = pango_color_span(tag_string, 'blue')
        att_string = ''
        for attrib in self.xml_root.attrib:
            name = clean_lxml_namespace(attrib, self.namespace_dic)
            val = self.xml_root.attrib[attrib]
            att_string += make_attribute_pango(name, val,
                                            self.attrib_color,
                                            self.value_color)
        root_string = ('%s%s%s%s') % (cgi.escape('<'),
                                     tag_string,
                                     att_string,
                                     cgi.escape('>'))
        
        path = etree.ElementTree(self.xml_root).getpath(self.xml_root)
        self.firstnode = self.treestore.append(None,
                                                [root_string,
                                                 path])
        # Reset node counter and activate cancel button
        self.counter.reset()
        self.cancel = False
        self.button_cancel.set_sensitive(True)        
        # Call recursive function to append xml elements
        self.recurse_append(self.firstnode, self.xml_root, self.namespace_dic)
        # Update the gui
        while gtk.events_pending():
            gtk.main_iteration()                        
        self.tvcolumn.set_title(self.xml_file + ' ('+ self.xml_filesize.human + ')')
        self.button_cancel.set_sensitive(False)        

    def drag_drop(self, widget, context, x, y, selection, target_type, timestamp):
        """ Drag and drop callback - modified from pygtk FAQ about file DND """
        if target_type == self.target_type_uri_list:
                uri = selection.data.strip('\r\n\x00')
                uri_list = uri.split()
                if len(uri_list) > 1: # can't handle multiple files...
                    print "multiple files. Can't do it"
                    return False
                else:
                    self.xml_file = uri_to_filename(uri_list[0])
                    self.xml_filesize.set_size(os.path.getsize(self.xml_file))
                    self.button_create.set_sensitive(True)
                    self.button_create.set_tooltip_text('Create XML tree')
                    self.button_create.emit('clicked')

    def __init__(self, initial_file=''):
        """ MainWindow initialization """
        self.xml_file = initial_file
        self.xml_filesize = FileSize.FileSize(0)

        self.cancel = False
        self.current_xpath = ''
        self.tag_color = 'blue'
        self.attrib_color = '#006F00'
        self.value_color = '#AA0000'
        self.counter = CounterClass(0)
        
        self.main_window = gtk.Window()
        self.main_window.set_size_request(700, 500)
        icon_path = os.path.join(sys.path[0], 'icon.png')
        self.main_window.set_icon_from_file(icon_path)
        self.main_window.connect("delete_event", self.delete_event)
        self.main_window.connect('key-press-event', self.capture_keypress)
        # Scrolled window for the tree    
        self.scrolled_window = gtk.ScrolledWindow()
        self.scrolled_window.set_border_width(10)
        self.scrolled_window.set_policy(gtk.POLICY_AUTOMATIC,
                                        gtk.POLICY_AUTOMATIC)
        # Create tree button            
        self.button_create = gtk.Button('Create Tree')
        self.button_create.connect("clicked", self.create_button_callback)
        self.button_create.set_tooltip_text('Chose XML file first')
        # File choose button
        self.button_choose = gtk.Button('Browse XML...')
        self.button_choose.connect("clicked", self.chose_file)
        self.button_choose.set_tooltip_text('Choose (load) XML file')
        # Horizontal box for buttons
        self.hbox =  gtk.HBox()
        self.hbox.pack_start(self.button_choose, True, True, 1)
        self.hbox.pack_end(self.button_create, True, True, 1)
        # Progress bar
        self.progressbar = gtk.ProgressBar(adjustment=None)
        self.progressbar.set_text(' ')
        # Cancel button (icon only)
        image = gtk.Image()
        settings = gtk.settings_get_default()
        settings.props.gtk_button_images = True
        image.set_from_stock(gtk.STOCK_CANCEL, gtk.ICON_SIZE_BUTTON)
        self.button_cancel = gtk.Button('')
        self.button_cancel.set_image(image)
        self.button_cancel.set_label('')
        self.button_cancel.connect('clicked', self.cancel_button_callback)
        self.button_cancel.set_sensitive(False)
        self.button_cancel.set_tooltip_text('Stop tree creation')
        # Horizontal box for progress bar and cancel 
        self.hbox_progress = gtk.HBox()
        self.hbox_progress.pack_start(self.progressbar, True, True, 1)
        self.hbox_progress.pack_start(self.button_cancel, False, False, 1)
        # Label and text entry for displaying path of node
        self.label_path = gtk.Label('xPath:')
        self.entry_path = gtk.Entry()
        self.entry_path.set_editable(False)
        # Horizontal box for xPath display
        self.hbox_path = gtk.HBox()
        self.hbox_path.pack_start(self.label_path, False, False, 2)
        self.hbox_path.pack_start(self.entry_path, True, True, 2)
        # Main vbox
        self.vbox_main = gtk.VBox()
        # Pack the the widgets in the main vbox
        self.vbox_main.pack_start(self.hbox, False, False, 1)
        self.vbox_main.pack_start(self.hbox_progress, False, False, 1)
        self.vbox_main.pack_start(self.scrolled_window, True, True, 0)
        self.vbox_main.pack_end(self.hbox_path, False, False, 1)
        # Add the vbox to the window
        self.main_window.add(self.vbox_main)
        self.main_window.set_title("gxmltree")
        # Create the TreeStore, TreeView and two columns
        self.treestore = gtk.TreeStore(str, str)     
        self.treeview = gtk.TreeView(self.treestore)
        self.treeview.connect("cursor-changed", self.tree_change_callback)
        self.treeview.show()
        self.tvcolumn = gtk.TreeViewColumn('Browse XML file, then press "Create Tree"')
        self.treeview.append_column(self.tvcolumn)
        self.cell = gtk.CellRendererText()
        self.tvcolumn.pack_start(self.cell, True)
        self.tvcolumn.add_attribute(self.cell, 'markup', 0)
        self.treeview.set_search_column(0)
        self.scrolled_window.add(self.treeview)
        # Show all the widgets and main_window
        self.treeview.show()
        self.scrolled_window.show()
        self.main_window.set_position(gtk.WIN_POS_CENTER)
        # Drag and drop
        self.target_type_uri_list = 80
        dnd_list = [ ( 'text/uri-list', 0, self.target_type_uri_list ) ]
        self.main_window.drag_dest_set(0, [], 0)
        self.main_window.connect('drag_data_received', self.drag_drop)
        self.main_window.drag_dest_set( gtk.DEST_DEFAULT_MOTION |
                            gtk.DEST_DEFAULT_HIGHLIGHT | gtk.DEST_DEFAULT_DROP,
                            dnd_list, gtk.gdk.ACTION_COPY
            )
        # show it!
        self.main_window.show_all()     
        # An xml can be passed as a command line argument thus also enabling
        # to do an open with... for xml files.
        if self.xml_file != '':
            self.xml_filesize.set_size(os.path.getsize(self.xml_file))
            self.button_create.set_sensitive(True)
            self.button_create.set_tooltip_text('Create XML tree')            
            self.button_create.emit('clicked')
        else:
            self.button_create.set_sensitive(False)
            self.xml_filesize.set_size(0)

def main():
    """ main function """
    gtk.main()   

if __name__ == "__main__":
    if len(sys.argv) > 1 and os.path.exists(sys.argv[1]):
        main_win = MainWindow(sys.argv[1])    
    else:
        main_win = MainWindow()
    main()
